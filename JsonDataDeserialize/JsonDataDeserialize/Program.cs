﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace JsonDataDeserialize
{
    public class Movie
    {
        public int id { get; set; }
        public string item { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string json = File.ReadAllText("../../../MoviesDB.json");
            ReturnStuff(json);
        }

        static void ReturnStuff(string json)
        {
            DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(json);
            DataTable dataTable = dataSet.Tables["Table1"];
            List<Movie> movieList = new List<Movie>();
            movieList = (from DataRow dr in dataTable.Rows
                         select new Movie()
                         {
                             id = Convert.ToInt32(dr["id"]),
                             item = dr["item"].ToString()
                         }).ToList();
            foreach (var x in movieList)
            {
                Console.WriteLine("id=" + x.id + " item=" + x.item);
            }
        }
    }
}
